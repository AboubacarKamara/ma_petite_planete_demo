// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:app_ma_petite_planete/Page/page_contact.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:app_ma_petite_planete/main.dart';


void main() {
  testWidgets('MyHomePage should render correctly', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: MyHomePage(title: 'Flutter Demo Home Page')));

    // Expect to find widgets on the page.
    expect(find.text('Page accueil'), findsOneWidget);
    expect(find.byType(AppBar), findsOneWidget);
    expect(find.byType(Image), findsOneWidget);
    expect(find.text("Page contact"), findsOneWidget);
    expect(find.byKey(const ValueKey("logo")), findsOneWidget);
  });

  testWidgets('Page contact button should navigate to PageContact', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: MyHomePage(title: 'Flutter Demo Home Page')));

    await tester.tap(find.byKey(const ValueKey("bouton page contact")));
    await tester.pumpAndSettle(const Duration(seconds: 5));
    expect(const PageContact(), isNotNull);
  });
}
