import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:app_ma_petite_planete/Page/page_contact.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  testWidgets('PageContact should render correctly', (WidgetTester tester) async {
    // Build our widget and trigger a frame.
    await tester.pumpWidget(const MaterialApp(home: PageContact()));

    // Expect to find widgets on the page.
    expect(find.text('Page Contact'), findsWidgets);
    expect(find.byType(AppBar), findsOneWidget);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(InkWell), findsWidgets);
    expect(find.byIcon(Icons.copy), findsWidgets);
  });

  testWidgets('test clipboard function', (WidgetTester tester) async {
    var dataCopied = false;
    
    final List<MethodCall> log = <MethodCall>[];
    SystemChannels.platform.setMockMethodCallHandler((MethodCall methodCall) async {
      log.add(methodCall);
    });

    await tester.pumpWidget(
      MaterialApp(
        home: Container(
          width: 10,
          height: 10,
          child: ElevatedButton(
            onPressed: (() {
              Clipboard.setData(ClipboardData(text: "test")).then((_) {
                dataCopied = true;
              });
            }),
            child: Text("Copy Text"),
          ),
        ),
      ),
    );

    await tester.tap(find.byType(ElevatedButton));
    expect(dataCopied, true);
  });
}
